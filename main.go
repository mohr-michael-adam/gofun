package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	controllerModel "bitbucket.org/mohr-michael-adam/gofun/controller/model"
	"bitbucket.org/mohr-michael-adam/gofun/db"
	dbModel "bitbucket.org/mohr-michael-adam/gofun/db/model"
	"github.com/gorilla/mux"
	"github.com/docopt/docopt-go"
)

const username = "<username>"
const password = "<password>"
const dbName = "<db>"

func parseArgs() map[string]interface{} {
	usage := `Having fun with go!

Usage:
  gofun <username> <password> <db>

Options:
  -h --help     Show this screen.
  --version     Show version.`

	arguments, _ := docopt.Parse(usage, nil, true, "gofun 0.01", false)
	return arguments
}

func main() {
	args := parseArgs()

	err := db.InitDb(args[username].(string), args[password].(string), args[dbName].(string))

	if err != nil {
		log.Fatalf("Unable to init the DB! %v", err)
	}

	fmt.Println("Hello world!")

	var db = db.Db

	user := dbModel.NewUser().SetUsername("mohr").SetPassword("pass")

	fmt.Println("User ID: ", user.ID)
	fmt.Println("Created At: ", user.CreatedAt)

	fmt.Println(fmt.Sprintf("%+v, %T", user, user))

	user2 := dbModel.User{}

	fmt.Println("User 2 ID:", user2.ID)

	db.Create(&user)

	db.First(&user)

	fmt.Println(fmt.Sprintf("%v", user))

	var m []dbModel.User

	m = make([]dbModel.User, 2)

	fmt.Println("Length: ", len(m))
	fmt.Println("Capacity: ", cap(m))

	db.Unscoped().Find(&m)

	fmt.Println("Length: ", len(m))
	fmt.Println("Capacity: ", cap(m))

	fmt.Println(m)

	db.Unscoped().Where("id LIKE ?", "23").Delete(dbModel.User{})

	//db.Unscoped().Delete(&m)

	fmt.Println("Goodbye world")

	r := mux.NewRouter()
	r.HandleFunc("/users", handler).Methods("POST")
	http.ListenAndServe(":8080", r)
}

func handler(w http.ResponseWriter, r *http.Request) {
	var db = db.Db

	fmt.Println("Body 123:")

	dec := json.NewDecoder(r.Body)

	u := controllerModel.NewUserNilled()
	if err := dec.Decode(u); err == io.EOF {
		// handle
		fmt.Println("none")
	} else if err != nil {
		// handle

		log.Println("Error")
		log.Println(err)
	}

	// needs validation

	log.Printf("%v", u)
	if u.ID != nil {
		log.Printf("%q", *u.ID)
	}

	// copy to DB model
	dbUser := dbModel.NewUser()

	dbUser.SetUsername(*u.Username)
	dbUser.SetPassword(*u.Password)

	fmt.Println(db)

	db.Create(dbUser)

	// Need converters

	u.SetID(dbUser.ID)
	u.CreatedAt = &dbUser.CreatedAt
	u.UpdatedAt = &dbUser.UpdatedAt

	inc := json.NewEncoder(w)

	inc.Encode(u)
}
