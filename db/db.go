package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"bitbucket.org/mohr-michael-adam/gofun/db/model"
)

var Db *gorm.DB

func InitDb(username, password, db string) error {

	var err error

	Db, err = gorm.Open("mysql", username + ":" + password + "@/" + db+"?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		return err
	}

	models := model.ModelsToMigrate()

	for _, model := range models {
		Db.AutoMigrate(model)
	}

	return nil
}
