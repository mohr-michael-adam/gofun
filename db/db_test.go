package db

import (
	"testing"
	"flag"
)

var (
	database = flag.Bool("database", false, "run database integration tests")
	username = flag.String("username", "username", "DB username")
	password = flag.String("password", "password", "DB password")
	db = flag.String("db", "db", "DB")
)

func TestInitDb(t *testing.T) {
	flag.Parse()

	if !*database {
		t.Skip()
	}

	err := InitDb(*username, *password, *db)

	if err != nil {
		t.Fail()
	}
}
