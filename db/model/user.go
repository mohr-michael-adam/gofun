package model

import (
	"github.com/satori/go.uuid"
	"time"
)

type User struct {
	ID        string `gorm:"type:char(36)"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Password  string `gorm:"size:255"`
	Username  string `gorm:"size:255",sql:"index"`
}

func (User) TableName() string {
	return "user"
}

func NewUser() *User {
	return &User{ID: uuid.NewV4().String()}
}

func (u *User) SetPassword(password string) *User {
	u.Password = password
	return u
}

func (u *User) SetUsername(username string) *User {
	u.Username = username
	return u
}

func (u *User) SetDefaultID() *User {
	u.ID = uuid.NewV4().String()
	return u
}
