package model

import (
	"time"
	"bytes"
	"fmt"
)

type User struct {
	// pointers are used as to be able to determine if the field has been initialized or not
	ID        *string
	CreatedAt *time.Time
	UpdatedAt *time.Time
	Password  *string
	Username  *string
}

func (u User) String() string {
	var buffer bytes.Buffer
	buffer.WriteString("User{ID:")
	if u.ID == nil {
		buffer.WriteString("nil")
	} else {
		buffer.WriteString(fmt.Sprintf("%q", *u.ID))
	}
	buffer.WriteString(" CreatedAt:")
	if u.CreatedAt == nil {
		buffer.WriteString("nil")
	} else {
		buffer.WriteString(fmt.Sprintf("%q", *u.CreatedAt))
	}
	buffer.WriteString(" UpdatedAt:")
	if u.UpdatedAt == nil {
		buffer.WriteString("nil")
	} else {
		buffer.WriteString(fmt.Sprintf("%q", *u.UpdatedAt))
	}
	buffer.WriteString(" Password:")
	if u.Password == nil {
		buffer.WriteString("nil")
	} else {
		buffer.WriteString(fmt.Sprintf("%q", *u.Password))
	}
	buffer.WriteString(" Username:")
	if u.Username == nil {
		buffer.WriteString("nil")
	} else {
		buffer.WriteString(fmt.Sprintf("%q", *u.Username))
	}
	buffer.WriteString("}")

	return buffer.String()
}

func (u *User) SetPassword(p string) *User {
	u.Password = &p
	return u
}

func (u *User) SetUsername(username string) *User {
	u.Username = &username
	return u
}

func (u *User) SetID(id string) *User {
	u.ID = &id
	return u
}

func NewUserNilled() *User {
	return &User{ID: nil, Password: nil, Username: nil, CreatedAt: nil, UpdatedAt: nil}
}
